import React from 'react';
import CreateNews from "./components/CreateNews/CreateNews";
import NewsList from "./components/NewsList/NewsList"




class App extends React.Component {
  constructor(props){
    super(props);


    this.state ={
      allNews:[],
      addNew: this.updateAllNews,
    }

    this.updateAllNews = this.updateAllNews.bind(this);
  

  }



  updateAllNews(item){  
    this.state.allNews.push(item)
    this.setState({
      allNews: this.state.allNews
    });
    
  }
  
  
  

  render() {
    return (
      <div className="App">
        <CreateNews addNew={this.updateAllNews}
          />
        <NewsList allNews={this.state.allNews}
          addNew={this.updateAllNews}
          />
        
      </div>

    )
  };
}

export default App;